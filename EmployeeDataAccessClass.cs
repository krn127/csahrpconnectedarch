﻿using System;
using System.Collections.Generic;
using DAL;

namespace BLL
{
    public class EmployeeDataAccessClass
    {
        EmployesDataClass empdataClass = new EmployesDataClass();
        public List<Employee> GetEmployees()
        {

            empdataClass = new EmployesDataClass();
            return empdataClass.getAllEmployees();
        }
        public Employee getAllEmployeeById(int id)
        {
            empdataClass = new EmployesDataClass();
            return empdataClass.getAllEmployeeById(id);

        }
         public string AddEmployee(string empName,string city, int mgrid,double salary,int deptid, long phoneNo)
        {
            empdataClass = new EmployesDataClass();
            if(empdataClass.InsertEmployees(empName,city,mgrid,salary,deptid,phoneNo)>0)
            {
                return "Data added successfully!!!!";
            }
            else
            {
                return "No Data Found!!";
            }
        }

        public string UpdateEmployee( double salary, string empName)
        {
            empdataClass = new EmployesDataClass();
            if (empdataClass.UpdateEmployees(salary, empName) >0)
            {
                return "Updated successfully!!!!";
            }
            else
            {
                return "Update failed!!";
            }
        }

        public string DeleteEmployee( string empName)
        {
            empdataClass = new EmployesDataClass();
            if (empdataClass.DeleteEmployees(empName) > 0)
            {
                return "Deleted successfully!!!!";
            }
            else
            {
                return "delete failed";
            }

 
        }
        public string AddEmployeeInEmployeeNew(int deptId, string empName,DateTime doj, float salary, string design)
        {
            empdataClass = new EmployesDataClass();
            if (empdataClass.InsertIntoEmployeeNew(deptId,empName,doj,salary,design) > 0)
            {
                return "Data added in new employees table  successfully!!!!";
            }
            else
            {
                return "No Data Found!!";
            }
        }
    }
}
