﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class Employee
    {
        public int EmpId{ get; set; }
        public string EmpName { get; set; }

        public double Salary { get; set; }
    }
}
