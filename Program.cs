﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;
using DAL;

namespace ConnectedArchitectureDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            EmployeeDataAccessClass EmpDataAccess = new EmployeeDataAccessClass();
            foreach(Employee emp in EmpDataAccess.GetEmployees() )

            {
               // Console.WriteLine("Employee info:");
                Console.WriteLine(emp.EmpId + "\t\t" + emp.EmpName + "\t\t" + emp.Salary);

            }
            Console.WriteLine("==================");
            Employee employee = EmpDataAccess.getAllEmployeeById(101);
            Console.WriteLine("Searched EMployee is:" + employee.EmpName);

            Console.WriteLine("======");
           // Console.WriteLine(EmpDataAccess.AddEmployee("john cena", "colombo", 102, 385000.00, 2, 8928439655));

            Console.WriteLine("======");
           //Console.WriteLine( EmpDataAccess.UpdateEmployee( 55500,"jack"));

            Console.WriteLine("=======");
           //Console.WriteLine(EmpDataAccess.DeleteEmployee("john cena"));

            Console.WriteLine("=======here");
            //Console.WriteLine( EmpDataAccess.AddEmployeeInEmployeeNew(102, "shanoy malhotra",Convert.ToDateTime("10/Jan/2020"), 40000, "manager"));
            // Console.WriteLine(EmpDataAccess.AddEmployeeInEmployeeNew(10, "Shruti", Convert.ToDateTime("10/Jan/98"), 4000, "Analyst"));
            // Console.WriteLine(EmpDataAccess.AddEmployeeInEmployeeNew(10, "shanthi", Convert.ToDateTime("12/Jun/98"), 6000, "Manager"));
            Console.WriteLine(EmpDataAccess.AddEmployeeInEmployeeNew(20, "Goodwill", Convert.ToDateTime("13/dec/99"),10000, "Senior analyst"));

        }
    }
}
