﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;

namespace DAL
{
    public class EmployesDataClass
    {
        SqlConnection sqlconnObj;
        SqlCommand commandObj;
        SqlDataReader sqlDataReader;
    public void Init()
        {
            sqlconnObj = new SqlConnection();
            sqlconnObj.ConnectionString = @"Data Source=.;Initial Catalog=NewDotnetDB;Integrated Security=false;User Id=sa;Password=cybage@123";
            //2.open connection

            sqlconnObj.Open();
            commandObj = new SqlCommand();

            commandObj.Connection = sqlconnObj;
            commandObj.CommandType = CommandType.Text;


        }
        public List<Employee>getAllEmployees()
        {
            List<Employee> employees = new List<Employee>();
            //coonected architecture==>
            //1.Establish a connection
            Init();
            
            
            
            //3.Create command object 
           

            commandObj.CommandText = "Select * from Employee";

            //4.Execute command

            SqlDataReader sqlDataReader= commandObj.ExecuteReader();

            while(sqlDataReader.Read())
            {
                employees.Add(new Employee
                {
                    EmpId=int.Parse(sqlDataReader[0].ToString()),
                    EmpName=sqlDataReader[1].ToString(),
                    Salary=Convert.ToDouble(sqlDataReader[4].ToString())
                }

                    );
            }

            //5.close tjhe connection

            sqlconnObj.Close();
            return employees;
        }
        public Employee getAllEmployeeById(int id)
        {
            Employee searchedEmployee = null;
        
            Init();



            //3.Create command object 


            commandObj.CommandText = "Select * from Employee where employeeId="+id;

            //4.Execute command

            SqlDataReader sqlDataReader = commandObj.ExecuteReader();

            while (sqlDataReader.Read())
            {
                searchedEmployee = new Employee
                {
                    EmpId = int.Parse(sqlDataReader[0].ToString()),
                    EmpName = sqlDataReader[1].ToString(),
                    Salary = Convert.ToDouble(sqlDataReader[4].ToString())
                };

                    
            }

            //5.close tjhe connection

            sqlconnObj.Close();
            return searchedEmployee ;
        }

        public int InsertEmployees(string empName, string City, int mgrid, double salary, int deptid, long phoneNo)
        {
       
            Init();
            int RowsAffected = 0;
            commandObj.CommandText="insert into Employee values('"+empName+"','"+City+"',"+mgrid+","+salary+","+deptid+","+phoneNo+")";

            RowsAffected = commandObj.ExecuteNonQuery();
            sqlconnObj.Close();
            return RowsAffected;
            

        }

        public int UpdateEmployees(double salary, string empName)
        {

            Init();
            int RowsAffected = 0;
            commandObj.CommandText ="update Employee set Salary='" + salary + "' where EmployeeName='"+ empName+"'" ;

            RowsAffected = commandObj.ExecuteNonQuery();
            sqlconnObj.Close();
            return RowsAffected;


        }

        public int DeleteEmployees(string empName)
        {

            Init();
            int RowsAffected = 0;
            commandObj.CommandText = "delete from Employee where EmployeeName= "+ empName+"";

            RowsAffected = commandObj.ExecuteNonQuery();
            sqlconnObj.Close();
            return RowsAffected;


        }
        public int InsertIntoEmployeeNew(int deptId,string empName, DateTime doj, float salary, string design)
        {

            Init();
            int RowsAffected = 0;
           // var cmd = new SqlCommand("insert into EmployeeNew(DeptId,EmpName,Doj,Salary,Design) values(@deptid,@empname,@doj,@salary,@design)");
            //cmd.Parameters.Add("@deptid", SqlDbType.Int);
           //// cmd.Parameters.Add("@empname", SqlDbType.VarChar);
           // cmd.Parameters.Add("@doj", SqlDbType.Date);
           // cmd.Parameters.Add("@salary", SqlDbType.Float);
           // cmd.Parameters.Add("@design", SqlDbType.VarChar);
             commandObj.CommandText = "insert into EmployeeNew values('" +deptId + "','" + empName + "','" +Convert.ToDateTime(doj) + "','" + salary + "','"+ design +"')";

           // commandObj = cmd;
            RowsAffected = commandObj.ExecuteNonQuery();
            sqlconnObj.Close();
            return RowsAffected;


        }


    }
}
